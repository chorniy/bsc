# Bsc

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Dependencies installation

Run `npm install` to install the dependencies.

## Development server

Run `npm run start` for a dev server. The app will run on `http://localhost:9000/` and will be open automatically in the default browser.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).
