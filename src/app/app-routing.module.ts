import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoteListComponent } from './pages/note-list/note-list.component';
import { NoteDetailComponent } from './pages/note-detail/note-detail.component';


const routes: Routes = [
  {
    path: '',
    component: NoteListComponent,
    pathMatch: 'full',
  },
  {
    path: 'notes/:id',
    component: NoteDetailComponent,
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
