import { TestBed } from '@angular/core/testing';

import { NoteService } from './note.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Note } from '../models/note.model';
import { environment } from '../../environments/environment';

describe('NoteService', () => {
  let service: NoteService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(NoteService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should load all notes', () => {
    service.getNotes().subscribe((notes: Note[]) => {
      expect(notes).toBeTruthy();
      expect(notes.length).toBe(2);
    });

    const req = httpTestingController.expectOne(`${environment.host}/notes`);

    expect(req.request.method).toBe('GET');
    req.flush([
      {
        id: 1,
        title: 'Buy bread',
      },
      {
        id: 2,
        title: 'Learn German',
      },
    ]);
  });

  it('should load note by id', () => {
    service.getNoteById(1).subscribe((note: Note) => {
      expect(note).toBeTruthy();
      expect(note.id).toBe(1);
    });

    const req = httpTestingController.expectOne(`${environment.host}/notes/1`);

    expect(req.request.method).toBe('GET');
    req.flush({
      id: 1,
      title: 'Buy bread',
    });
  });

  it('should create a note', () => {
    service.addNote('Clean up').subscribe((note: Note) => {
      expect(note).toBeTruthy();
      expect(note.title).toContain('Clean up');
    });

    const req = httpTestingController.expectOne(`${environment.host}/notes`);

    expect(req.request.method).toBe('POST');
    req.flush({
      id: 1,
      title: 'Clean up',
    });
  });

  it('should update a note', () => {
    service.updateNote(1,  {title: 'Cook something'}).subscribe((note: Note) => {
      expect(note).toBeTruthy();
      expect(note.id).toBe(1);
      expect(note.title).toContain('Cook something');
    });

    const req = httpTestingController.expectOne(`${environment.host}/notes/1`);

    expect(req.request.method).toBe('PUT');
    expect(req.request.body.title).toContain('Cook something');
    req.flush({
      id: 1,
      title: 'Cook something',
    });
  });

  it('should delete a note', () => {
    service.deleteNote(1).subscribe((response) => {
      expect(response).toBeFalsy();
    });

    const req = httpTestingController.expectOne(`${environment.host}/notes/1`);

    expect(req.request.method).toBe('DELETE');
    req.flush(null);
  });

  afterEach(() => {
    httpTestingController.verify();
  });
});
