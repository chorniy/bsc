import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Note } from '../models/note.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  /**
   * Get all notes
   */
  getNotes(): Observable<Note[]> {
    return this.httpClient.get<Note[]>(environment.host + '/notes');
  }

  /**
   * Get a note by id
   */
  getNoteById(id: Note['id']): Observable<Note> {
    return this.httpClient.get<Note>(`${environment.host}/notes/${id}`);
  }

  /**
   * Create a new note
   * @param title
   */
  addNote(title: Note['title']): Observable<Note> {
    return this.httpClient.post<Note>(`${environment.host}/notes`, {title});
  }

  /**
   * Update a note
   * @param id
   * @param updatedNote
   */
  updateNote(id: Note['id'], updatedNote: Partial<Note>): Observable<Note> {
    return this.httpClient.put<Note>(`${environment.host}/notes/${id}`, updatedNote);
  }

  /**
   * Delete a note
   * @param id
   */
  deleteNote(id: Note['id']): Observable<void> {
    return this.httpClient.delete<void>(`${environment.host}/notes/${id}`);
  }
}
