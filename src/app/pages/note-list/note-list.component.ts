import { Component, OnInit } from '@angular/core';
import { NoteService } from '../../providers/note.service';
import { Note } from '../../models/note.model';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogOption } from '../../enums/confirm-dialog-option.enum';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss'],
})
export class NoteListComponent implements OnInit {
  /** List of notes */
  notes: Note[];
  /** New note control */
  newNoteControl = new FormControl('', Validators.required);

  constructor(
    private noteService: NoteService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.loadNotes();
  }

  /**
   * Load all notes.
   */
  loadNotes(): void {
    this.noteService.getNotes().subscribe((notes: Note[]) => {
      this.notes = notes;
    });
  }

  /**
   * Create a note and update the view by reloading the notes.
   */
  saveNote(): void {
    this.noteService.addNote(this.newNoteControl.value.trim()).subscribe(() => {
      this.snackBar.open(this.translateService.instant('SNACKS.CREATED'));
      this.newNoteControl.reset();
      this.loadNotes();
    });
  }

  /**
   * Open dialog for user to confirm the note deletion.
   * @param id
   */
  openDeleteDialog(id: Note['id']) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result === ConfirmDialogOption.YES) {
        this.deleteNote(id);
      }
    });
  }

  /**
   * Delete selected note and update the view by reloading the notes.
   * @param id
   */
  deleteNote(id: Note['id']): void {
    this.noteService.deleteNote(id).subscribe(() => {
      this.snackBar.open(this.translateService.instant('SNACKS.DELETED'));
      this.loadNotes();
    });
  }

  /**
   * Function to track by.
   * Needed for performance in order to keep unchanged items in the view without re-rendering.
   * @param index
   * @param item
   */
  trackByFn(index: number, item: Note) {
    return item.id;
  }
}
