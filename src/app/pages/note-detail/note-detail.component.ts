import { Component, OnInit } from '@angular/core';
import { Note } from '../../models/note.model';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, take } from 'rxjs/operators';
import { NoteService } from '../../providers/note.service';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogOption } from '../../enums/confirm-dialog-option.enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-note-detail',
  templateUrl: './note-detail.component.html',
  styleUrls: ['./note-detail.component.scss'],
})
export class NoteDetailComponent implements OnInit {
  /** Current note */
  note: Note;
  /** Form control with note's title as value */
  noteControl = new FormControl('', Validators.required);

  constructor(
    private activatedRoute: ActivatedRoute,
    private noteService: NoteService,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit(): void {
    this.loadNote();
  }

  /**
   * Load the note by id given in the params.
   */
  loadNote(): void {
    this.activatedRoute.paramMap.pipe(
      take(1),
      switchMap(params => this.noteService.getNoteById(+params.get('id'))),
    ).subscribe((note) => {
      this.note = note;
      this.noteControl.reset(this.note.title);
    });
  }

  /**
   * Change the note and update the view by reloading the note.
   */
  saveNote(): void {
    this.noteService.updateNote(
      this.note.id,
      {title: this.noteControl.value.trim()},
    ).subscribe(() => {
      this.snackBar.open(this.translateService.instant('SNACKS.CHANGED'));
      this.loadNote();
    });
  }

  /**
   * Open dialog for user to confirm the note deletion.
   */
  openDeleteDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result === ConfirmDialogOption.YES) {
        this.deleteNote();
      }
    });
  }

  /**
   * Delete the note and navigate to the list.
   */
  deleteNote(): void {
    this.noteService.deleteNote(this.note.id).subscribe(() => {
      this.snackBar.open(this.translateService.instant('SNACKS.DELETED'));
      this.router.navigate(['/']);
    });
  }
}
