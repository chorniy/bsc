export enum ConfirmDialogOption {
  YES = 'yes',
  NO = 'no',
}
