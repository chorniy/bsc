import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ConfirmDialogOption } from '../../enums/confirm-dialog-option.enum';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
  ) { }

  /**
   * Confirm deletion.
   */
  delete() {
    this.close(ConfirmDialogOption.YES);
  }

  /**
   * Close the dialog with the passed parameters.
   * @param option - Data to pass to dialog emitter
   */
  close(option?: ConfirmDialogOption) {
    this.dialogRef.close(option);
  }
}
