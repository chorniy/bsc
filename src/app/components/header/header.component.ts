import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  /** Title of the current page */
  @Input() title: string;
  /** Show or hide back button */
  @Input() hasBackButton = false;

  constructor(
    private translateService: TranslateService,
    private snackBar: MatSnackBar,
  ) {
  }

  /**
   * Change the language for the whole application
   * @param code - Language code
   */
  changeLanguage(code: 'cs' | 'en'): void {
    this.translateService.use(code).subscribe(() => {
      this.snackBar.open(this.translateService.instant('SNACKS.LANGUAGE', {value: code.toUpperCase()}));
    });
  }
}
